FROM alpine:3.18 as builder
ARG TMUX_VERSION=master

RUN apk update && apk upgrade
RUN apk add --no-cache \ 
  automake=1.16.5-r2 \
  autoconf=2.71-r2 \
  bison=3.8.2-r1 \
  gcc=12.2.1_git20220924-r10 \
  git=2.40.1-r0 \
  linux-headers=6.3-r0 \
  libevent-dev=2.1.12-r6 \
  libevent-static=2.1.12-r6 \
  musl-dev=1.2.4-r2 \ 
  musl-utils=1.2.4-r2 \
  make=4.4.1-r1 tar=1.34-r3  \
  ncurses-dev=6.4_p20230506-r0 \
  ncurses-static=6.4_p20230506-r0 \
  pkgconf=1.9.5-r0 \
  utf8proc-dev=2.8.0-r0 \
  utf8proc=2.8.0-r0 \
  libutempter-dev=1.2.1-r8 \
  libutempter=1.2.1-r8 \
  && git clone -c "advice.detachedHead=false" --depth 1 --branch "${TMUX_VERSION}" https://github.com/tmux/tmux.git tmux

WORKDIR /tmux
RUN sh autogen.sh && ./configure --enable-static --enable-sixel --enable-utf8proc --enable-utempter
RUN make -j"$(nproc)"
RUN strip ./tmux

FROM scratch
COPY --from=builder /tmux/tmux /.
