# How to use this container

1. Download jmusicbot from <https://github.com/jagrosh/MusicBot> into this
   folder. At the time of writing, this is
   <https://github.com/jagrosh/MusicBot/releases/download/0.3.9/JMusicBot-0.3.9.jar>
2. Edit `config.txt` to have your bot's token and your owner id. See
   <https://jmusicbot.com/config/> for other config options
3. Edit the `dockerfile`, replacing the paths to the jar and config files with
   your paths. This will ensure they are copied into the container and the
   container has no dependencies on files on the host system.
4. Build the image: `docker build -t jmusicbot .`
5. Run the container: `docker-compose up -d`
