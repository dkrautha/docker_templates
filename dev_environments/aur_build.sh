#!/usr/bin/env sh

set -euo

if [ -z "$1" ]; then
	echo "Usage: $0 <package_name>"
	exit 1
fi

for pkg in "$@"; do
	[ -d "${pkg}" ] || git clone https://aur.archlinux.org/"${pkg}".git /tmp/"${pkg}"
	(cd "/tmp/${pkg}" && makepkg -s --noconfirm)
done
