#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

no_cache=""
if [[ $# -gt 0 && "$1" == "--no-cache" ]]; then
	no_cache="--no-cache"
fi

podman build --jobs 0 "${no_cache}" --pull=newer -t arch_base -f arch_base.Dockerfile
podman build "${no_cache}" --pull=newer -t arch_opencv -f arch_opencv.Dockerfile

distrobox assemble create --replace --verbose
