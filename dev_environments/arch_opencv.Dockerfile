# hadolint ignore=DL3007
FROM arch_base:latest

RUN --mount=type=cache,target=/var/cache/pacman/pkg \
  paru -S --needed --noconfirm opencv python-opencv
