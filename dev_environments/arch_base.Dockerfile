# hadolint ignore=DL3007
FROM quay.io/toolbx-images/archlinux-toolbox:latest as AUR_BUILDER

RUN pacman -Syu --noconfirm reflector \
  && reflector --country US --protocol https --sort rate --save /etc/pacman.d/mirrorlist 

ARG AUR_USER=snep
ARG AUR_USER_HOME="/var/${AUR_USER}"
RUN useradd "${AUR_USER}" --system --shell /usr/bin/nologin --create-home --home-dir "${AUR_USER_HOME}" \
  && passwd --lock "${AUR_USER}" \
  && echo "${AUR_USER} ALL=(ALL) NOPASSWD: /usr/bin/pacman" > "/etc/sudoers.d/allow_${AUR_USER}_to_pacman"

COPY ./aur_build.sh /
USER ${AUR_USER}

FROM AUR_BUILDER as BINS_BUILDER
RUN bash aur_build.sh paru-bin topgrade-bin clipboard-bin czkawka-gui-bin

FROM AUR_BUILDER as RSGAIN_BUILDER
RUN bash aur_build.sh rsgain 

FROM AUR_BUILDER as RR_BUILDER
RUN bash aur_build.sh rr

FROM AUR_BUILDER as BFG_BUILDER
RUN bash aur_build.sh bfg

FROM AUR_BUILDER as IWYU_BUILDER
RUN bash aur_build.sh include-what-you-use

FROM quay.io/toolbx-images/archlinux-toolbox:latest

# pacman config
RUN echo "Color" >> /etc/pacman.conf \
  && echo "ParallelDownloads = 5" >> /etc/pacman.conf \
  # update package repository
  && pacman -Syu --noconfirm reflector \
  # update mirrorlist
  && reflector --country US --protocol https --sort rate --save /etc/pacman.d/mirrorlist \
  # generate locale
  && sed -i 's/^#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen \
  && locale-gen \
  # symlinks for host programs
  && ln -sf /usr/bin/distrobox-host-exec /usr/local/bin/xdg-open \
  && ln -sf /usr/bin/distrobox-host-exec /usr/local/bin/podman \
  && ln -sf /usr/bin/distrobox-host-exec /usr/local/bin/distrobox \
  && ln -sf /usr/bin/distrobox-host-exec /usr/local/bin/flatpak 

COPY --from=BINS_BUILDER /tmp/**/*.pkg.tar.zst /tmp
COPY --from=RSGAIN_BUILDER /tmp/**/*.pkg.tar.zst /tmp
COPY --from=RR_BUILDER /tmp/**/*.pkg.tar.zst /tmp
COPY --from=BFG_BUILDER /tmp/**/*.pkg.tar.zst /tmp
COPY --from=IWYU_BUILDER /tmp/**/*.pkg.tar.zst /tmp

RUN --mount=type=cache,target=/var/cache/pacman/pkg \
  pacman -U --noconfirm /tmp/*.pkg.tar.zst \
  && rm -rf /tmp/*

RUN --mount=type=cache,target=/var/cache/pacman/pkg \
  paru -S --needed --noconfirm \
  # shell 
  fish \ 
  fisher \
  starship \
  zoxide \
  # terminal
  foot \
  foot-terminfo \
  # editor
  neovim \
  luarocks \
  python-pynvim \
  tree-sitter-cli \
  tmux \
  wl-clipboard \
  zellij \
  # git
  git \
  git-delta \
  gitui \
  lazygit \
  # cli tools
  b3sum \
  bat \
  bottom \
  broot \
  chafa \
  chezmoi \
  eza \
  fd \
  fzf \
  glow \
  gdu \
  hexyl \
  hyperfine \
  just \
  jq \
  kondo \
  neofetch \
  onefetch \
  procs \
  ripgrep \
  sad \
  sd \
  tealdeer \
  tokei \
  trash-cli \
  watchexec \
  zola \    
  # programming languages
  # rust
  bacon \
  cargo-audit \
  cargo-binstall \
  cargo-expand \
  cargo-flamegraph \
  cargo-msrv \
  cargo-nextest \
  cargo-sort \
  cargo-tarpaulin \
  cargo-update \
  evcxr_repl \
  rustup \
  # python
  python \
  python-pipx \
  nuitka \
  pypy3 \
  # cpp
  clang \
  cmake \
  cppcheck \
  meson \
  mold \
  ninja \
  sccache \
  upx \
  # java
  jdk-openjdk \
  gradle \
  # javascript
  nodejs \
  npm \
  deno \
  typescript \
  # typst
  typst \
  # czkawka dependencies
  libheif \
  webp-pixbuf-loader \
  # things distrobox installs in it's init script
  bash-completion \
  bc \
  curl \
  diffutils \
  findutils \
  glibc \
  gnupg \
  inetutils \
  keyutils \
  less \
  lsof \
  man-db \
  man-pages \
  mlocate \
  mtr \
  ncurses \
  nss-mdns \
  openssh \
  pigz \
  pinentry \
  procps-ng \
  rsync \
  shadow \
  sudo \
  tcpdump \
  time \
  traceroute \
  tree \
  tzdata \
  unzip \
  util-linux \
  util-linux-libs \
  vte-common \
  wget \
  words \
  xorg-xauth \
  zip \
  mesa \
  opengl-driver \
  vulkan-intel \
  vte-common \
  vulkan-radeon

RUN paru -Syu --noconfirm
